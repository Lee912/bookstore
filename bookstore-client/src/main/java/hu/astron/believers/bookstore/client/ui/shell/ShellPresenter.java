package hu.astron.believers.bookstore.client.ui.shell;

import com.github.mvp4g.mvp4g2.core.ui.AbstractPresenter;
import com.github.mvp4g.mvp4g2.core.ui.IsShell;
import com.github.mvp4g.mvp4g2.core.ui.annotation.EventHandler;
import com.github.mvp4g.mvp4g2.core.ui.annotation.Presenter;

import hu.astron.believers.bookstore.client.BookstoreEventBus;

@Presenter(viewClass = ShellView.class, viewInterface = IsShellView.class)
public class ShellPresenter extends AbstractPresenter<BookstoreEventBus, IsShellView>
        implements IsShellView.Presenter, IsShell<BookstoreEventBus, IsShellView> {

    public ShellPresenter() {
    }

    @Override
    public void onBeforeEvent(String eventName) {
    }

    @Override
    public void setShell() {
    }

    @EventHandler
    public void onStart() {
    }

    @EventHandler
    public void onInitHistory() {
    }
}
