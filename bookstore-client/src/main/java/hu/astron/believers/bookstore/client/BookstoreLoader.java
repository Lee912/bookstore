package hu.astron.believers.bookstore.client;

import com.github.mvp4g.mvp4g2.core.application.IsApplicationLoader;

/**
 * A applicaiton loader of the Mvp4g2MailApplication
 */
public class BookstoreLoader implements IsApplicationLoader {

    @Override
    public void load(FinishLoadCommand finishLoadCommand) {
        finishLoadCommand.finishLoading();
    }
}
