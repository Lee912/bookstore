package hu.astron.believers.bookstore.client;

import com.google.gwt.core.client.EntryPoint;

public class Bookstore implements EntryPoint {

    @Override
    public void onModuleLoad() {
        BookstoreApplication application = new BookstoreApplicationImpl();
        application.run();
    }
}
