package hu.astron.believers.bookstore.client.model;

import gwt.material.design.client.base.SearchObject;

@SuppressWarnings("serial")
public class Book extends SearchObject {

    private String name;

    private String description;

    private int power;

    public Book() {
    }

    public Book(String name, String description, int power) {
        super.setKeyword(name);
        this.name = name;
        this.description = description;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
