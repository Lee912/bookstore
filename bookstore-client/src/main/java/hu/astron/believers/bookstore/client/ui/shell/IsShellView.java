package hu.astron.believers.bookstore.client.ui.shell;

import com.github.mvp4g.mvp4g2.core.ui.IsLazyReverseView;

public interface IsShellView extends IsLazyReverseView<IsShellView.Presenter> {

    interface Presenter {

    }
}
