package hu.astron.believers.bookstore.client;

import com.github.mvp4g.mvp4g2.core.application.IsApplication;
import com.github.mvp4g.mvp4g2.core.application.annotation.Application;

@Application(eventBus = BookstoreEventBus.class,
        loader = BookstoreLoader.class,
        historyOnStart = true)
interface BookstoreApplication extends IsApplication {
}
