# Bookstore

Example bookstore using Wildfly, GWT, docker

###### Prerequisites
* JDk 1.8+ (Tested with 1.8)
* maven (Tested with 3.5.3)

###### Running the application
* Without preinstalled wildfly: mvn clean package wildfly:run
* With preinstalled wildfly: mvn clean package wildfly:run -Djboss-as.home="path to wildfly"
